#!/usr/bin/env python

import wx
import wx.lib.agw.aui as aui
import wx.lib.mixins.inspection as wit

import matplotlib as mpl
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure

from numpy import arange, sin, pi


import GPSCPY_UI

import threading
import serial
import serial.tools.list_ports
import GPSCPY_COM


class Plot(wx.Panel):
    def __init__(self, parent, id=-1, dpi=None, **kwargs):
        wx.Panel.__init__(self, parent, id=id, **kwargs)
        self.figure = mpl.figure.Figure(dpi=dpi, figsize=(2, 2))
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, 1, wx.EXPAND)
        sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)


class PlotNotebook(wx.Panel):
    def __init__(self, parent, id=-1):
        wx.Panel.__init__(self, parent, id=id)
        self.nb = aui.AuiNotebook(self)
        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.SetSizer(sizer)

    def add(self, name="plot"):
        page = Plot(self.nb)
        self.nb.AddPage(page, name)
        return page.figure



class CanvasPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(self.sizer)
        self.Fit()

    def draw(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2 * pi * t)
        self.axes.plot(t, s)



class GPSCPY_APP(wx.App):
    def OnInit(self):
        #GPSCPY_MAIN_FRAME=GPSCPY_UI.GPSCPY_FRAME_MAIN(None)
        GPSCPY_MAIN_FRAME=GPSCPY_UI.GPSCPYframe(None)
        
        panel = CanvasPanel(GPSCPY_MAIN_FRAME.DATA_GRAPH_PANEL)
        panel.draw()
        
        GPSCPY_MAIN_FRAME.Show(True)
        return True


if __name__ == '__main__':
    app = GPSCPY_APP()
    ##COM debug

    COM_PORTS=GPSCPY_COM.COM_PORT_LIST()
    BCON=[]
    COM_BAUDR=[]
    
    print(GPSCPY_COM.COM_INFO_LIST())
    print(COM_PORTS)

    for i in range(0,len(COM_PORTS)):
        BCON.append(False)
        COM_BAUDR.append(115200)
        
    #app.COM_DEVICES1Choices = [ u"COM1✅", u"COM2", u"COM3✅", u"COM4", u"COM5", u"COM6" ]
    #app.GPSCPY_MAIN_FRAME.COM_DEVICES1Choices=COM_PORTS
    #thread = threading.Thread(target=read_from_port, args=(serial_port,BCON,))
    #thread.start()
    ##COM debug
    
    
    app.MainLoop()
    
            
        

