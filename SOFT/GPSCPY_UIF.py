# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class GPSCPY_FRAME_MAIN
###########################################################################

class GPSCPY_FRAME_MAIN ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"GPSCPY", pos = wx.DefaultPosition, size = wx.Size( 990,680 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		self.MENU_B = wx.MenuBar( 0 )
		self.SetMenuBar( self.MENU_B )

		GPSCPY_CC = wx.BoxSizer( wx.VERTICAL )

		SYS_OPTIONS = wx.BoxSizer( wx.HORIZONTAL )

		self.SAMPLER = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.uPOS = wx.Panel( self.SAMPLER, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer8 = wx.BoxSizer( wx.VERTICAL )

		gbSizer1 = wx.GridBagSizer( 0, 0 )
		gbSizer1.SetFlexibleDirection( wx.BOTH )
		gbSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT13 = wx.StaticText( self.uPOS, wx.ID_ANY, u"XD ", wx.DefaultPosition, wx.Size( 37,-1 ), 0 )
		self.uPOS_TEXT13.Wrap( -1 )

		self.uPOS_TEXT13.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		gbSizer1.Add( self.uPOS_TEXT13, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.m_spinCtrlDouble1 = wx.SpinCtrlDouble( self.uPOS, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, -20, 20, 0, 0.1 )
		self.m_spinCtrlDouble1.SetDigits( 5 )
		self.m_spinCtrlDouble1.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		gbSizer1.Add( self.m_spinCtrlDouble1, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )

		self.uPOS_TEXT122 = wx.StaticText( self.uPOS, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.uPOS_TEXT122.Wrap( -1 )

		self.uPOS_TEXT122.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		gbSizer1.Add( self.uPOS_TEXT122, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )


		bSizer8.Add( gbSizer1, 3, wx.EXPAND, 2 )

		gbSizer14 = wx.GridBagSizer( 0, 0 )
		gbSizer14.SetFlexibleDirection( wx.BOTH )
		gbSizer14.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT134 = wx.StaticText( self.uPOS, wx.ID_ANY, u"XR ", wx.DefaultPosition, wx.Size( 37,-1 ), 0 )
		self.uPOS_TEXT134.Wrap( -1 )

		self.uPOS_TEXT134.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )
		self.uPOS_TEXT134.Enable( False )

		gbSizer14.Add( self.uPOS_TEXT134, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.m_spinCtrlDouble11 = wx.SpinCtrlDouble( self.uPOS, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 100, 0, 0.1 )
		self.m_spinCtrlDouble11.SetDigits( 5 )
		self.m_spinCtrlDouble11.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )
		self.m_spinCtrlDouble11.Enable( False )

		gbSizer14.Add( self.m_spinCtrlDouble11, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )

		self.uPOS_TEXT1223 = wx.StaticText( self.uPOS, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.uPOS_TEXT1223.Wrap( -1 )

		self.uPOS_TEXT1223.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )
		self.uPOS_TEXT1223.Enable( False )

		gbSizer14.Add( self.uPOS_TEXT1223, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )


		bSizer8.Add( gbSizer14, 3, wx.EXPAND, 5 )

		bSizer25 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button9 = wx.Button( self.uPOS, wx.ID_ANY, u"<<", wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		bSizer25.Add( self.m_button9, 0, wx.ALL, 5 )

		self.m_button10 = wx.Button( self.uPOS, wx.ID_ANY, u"G28", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer25.Add( self.m_button10, 1, wx.ALL, 5 )


		bSizer8.Add( bSizer25, 2, wx.EXPAND, 5 )


		self.uPOS.SetSizer( bSizer8 )
		self.uPOS.Layout()
		bSizer8.Fit( self.uPOS )
		self.SAMPLER.AddPage( self.uPOS, u"μPOS", True )
		self.FFRiS_B = wx.Panel( self.SAMPLER, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.SAMPLER.AddPage( self.FFRiS_B, u"FFRiS-B", False )
		self.FFRiS_P = wx.Panel( self.SAMPLER, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.SAMPLER.AddPage( self.FFRiS_P, u"FFRiS_P", False )

		SYS_OPTIONS.Add( self.SAMPLER, 0, wx.TOP|wx.BOTTOM|wx.LEFT|wx.EXPAND, 5 )

		self.MEASURE = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.SCO_MEAS = wx.Panel( self.MEASURE, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer23 = wx.BoxSizer( wx.VERTICAL )

		gbSizer16 = wx.GridBagSizer( 0, 0 )
		gbSizer16.SetFlexibleDirection( wx.BOTH )
		gbSizer16.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText24 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText24.Wrap( -1 )

		gbSizer16.Add( self.m_staticText24, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )

		self.m_staticText25 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )

		gbSizer16.Add( self.m_staticText25, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )

		self.m_staticText26 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )

		gbSizer16.Add( self.m_staticText26, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )


		bSizer23.Add( gbSizer16, 1, wx.EXPAND, 5 )

		gbSizer1321 = wx.GridBagSizer( 0, 0 )
		gbSizer1321.SetFlexibleDirection( wx.BOTH )
		gbSizer1321.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT13323 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"DISP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT13323.Wrap( -1 )

		gbSizer1321.Add( self.uPOS_TEXT13323, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_sCONTRL12221 = wx.SpinCtrl( self.SCO_MEAS, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0, -20, 20, 0 )
		gbSizer1321.Add( self.uPOS_sCONTRL12221, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_TEXT133212 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT133212.Wrap( -1 )

		gbSizer1321.Add( self.uPOS_TEXT133212, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer23.Add( gbSizer1321, 1, wx.EXPAND, 5 )

		gbSizer1322 = wx.GridBagSizer( 0, 0 )
		gbSizer1322.SetFlexibleDirection( wx.BOTH )
		gbSizer1322.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT13324 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"DISP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT13324.Wrap( -1 )

		gbSizer1322.Add( self.uPOS_TEXT13324, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_sCONTRL12222 = wx.SpinCtrl( self.SCO_MEAS, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0, -20, 20, 0 )
		gbSizer1322.Add( self.uPOS_sCONTRL12222, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_TEXT133213 = wx.StaticText( self.SCO_MEAS, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT133213.Wrap( -1 )

		gbSizer1322.Add( self.uPOS_TEXT133213, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer23.Add( gbSizer1322, 1, wx.EXPAND, 5 )

		bSizer24 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button27 = wx.Button( self.SCO_MEAS, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer24.Add( self.m_button27, 0, wx.ALL, 5 )

		self.m_button28 = wx.Button( self.SCO_MEAS, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer24.Add( self.m_button28, 0, wx.ALL, 5 )


		bSizer23.Add( bSizer24, 1, wx.EXPAND, 5 )


		self.SCO_MEAS.SetSizer( bSizer23 )
		self.SCO_MEAS.Layout()
		bSizer23.Fit( self.SCO_MEAS )
		self.MEASURE.AddPage( self.SCO_MEAS, u"SCO", True )
		self.nanoVNA_MEAS = wx.Panel( self.MEASURE, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.MEASURE.AddPage( self.nanoVNA_MEAS, u"nnVNA", False )
		self.FFRiS_MEAS = wx.Panel( self.MEASURE, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.MEASURE.AddPage( self.FFRiS_MEAS, u"FFRiS", False )

		SYS_OPTIONS.Add( self.MEASURE, 0, wx.TOP|wx.BOTTOM|wx.LEFT|wx.EXPAND, 5 )

		self.COORDT = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.Secuencer = wx.Panel( self.COORDT, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer19 = wx.BoxSizer( wx.VERTICAL )

		gbSizer132 = wx.GridBagSizer( 0, 0 )
		gbSizer132.SetFlexibleDirection( wx.BOTH )
		gbSizer132.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT1332 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"DISP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT1332.Wrap( -1 )

		gbSizer132.Add( self.uPOS_TEXT1332, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_sCONTRL1222 = wx.SpinCtrl( self.Secuencer, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0, -20, 20, 0 )
		gbSizer132.Add( self.uPOS_sCONTRL1222, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_TEXT13321 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT13321.Wrap( -1 )

		gbSizer132.Add( self.uPOS_TEXT13321, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer19.Add( gbSizer132, 1, wx.EXPAND, 5 )

		gbSizer133 = wx.GridBagSizer( 0, 0 )
		gbSizer133.SetFlexibleDirection( wx.BOTH )
		gbSizer133.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT1333 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"STEP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT1333.Wrap( -1 )

		gbSizer133.Add( self.uPOS_TEXT1333, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_sCONTRL1223 = wx.SpinCtrl( self.Secuencer, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0, -20, 20, 0 )
		gbSizer133.Add( self.uPOS_sCONTRL1223, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_TEXT13322 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"N", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT13322.Wrap( -1 )

		gbSizer133.Add( self.uPOS_TEXT13322, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer19.Add( gbSizer133, 1, wx.EXPAND, 5 )

		gbSizer134 = wx.GridBagSizer( 0, 0 )
		gbSizer134.SetFlexibleDirection( wx.BOTH )
		gbSizer134.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uPOS_TEXT1334 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"SAMP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT1334.Wrap( -1 )

		gbSizer134.Add( self.uPOS_TEXT1334, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_sCONTRL1224 = wx.SpinCtrl( self.Secuencer, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0, -20, 20, 0 )
		gbSizer134.Add( self.uPOS_sCONTRL1224, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		self.uPOS_TEXT133211 = wx.StaticText( self.Secuencer, wx.ID_ANY, u"[mm]", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.uPOS_TEXT133211.Wrap( -1 )

		gbSizer134.Add( self.uPOS_TEXT133211, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer19.Add( gbSizer134, 1, wx.EXPAND, 5 )

		bSizer2411 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button13 = wx.Button( self.Secuencer, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2411.Add( self.m_button13, 0, wx.ALL, 5 )

		self.m_button14 = wx.Button( self.Secuencer, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2411.Add( self.m_button14, 0, wx.ALL, 5 )


		bSizer19.Add( bSizer2411, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )


		self.Secuencer.SetSizer( bSizer19 )
		self.Secuencer.Layout()
		bSizer19.Fit( self.Secuencer )
		self.COORDT.AddPage( self.Secuencer, u"Secuencer", False )

		SYS_OPTIONS.Add( self.COORDT, 0, wx.ALL|wx.EXPAND, 5 )

		self.DEVICES_CONN = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.COM_OPTIONS = wx.Panel( self.DEVICES_CONN, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		COM_OPTIONS_SIZER = wx.BoxSizer( wx.VERTICAL )

		COM_LIST_SIZER = wx.BoxSizer( wx.HORIZONTAL )

		COM_LIST_SIZER.SetMinSize( wx.Size( -1,70 ) )
		COM_DEVICESChoices = [ u"COM1✅", u"COM2", u"COM3✅", u"COM4", u"COM5", u"COM6" ]
		self.COM_DEVICES = wx.ListBox( self.COM_OPTIONS, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,70 ), COM_DEVICESChoices, 0 )
		COM_LIST_SIZER.Add( self.COM_DEVICES, 0, wx.ALL, 5 )

		COM_LIST_BUTTONS_SIZER = wx.BoxSizer( wx.VERTICAL )

		self.COM_RELOAD_BUTTON = wx.Button( self.COM_OPTIONS, wx.ID_ANY, u"Reload", wx.DefaultPosition, wx.DefaultSize, 0 )
		COM_LIST_BUTTONS_SIZER.Add( self.COM_RELOAD_BUTTON, 1, wx.ALL, 5 )

		self.COM_CONNECT_BUTTON = wx.Button( self.COM_OPTIONS, wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		COM_LIST_BUTTONS_SIZER.Add( self.COM_CONNECT_BUTTON, 1, wx.ALL, 5 )


		COM_LIST_SIZER.Add( COM_LIST_BUTTONS_SIZER, 1, wx.EXPAND, 5 )


		COM_OPTIONS_SIZER.Add( COM_LIST_SIZER, 2, wx.EXPAND, 5 )

		BAUD_R_SIZER = wx.GridBagSizer( 0, 0 )
		BAUD_R_SIZER.SetFlexibleDirection( wx.BOTH )
		BAUD_R_SIZER.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.BAUD_RATE_TITLE = wx.StaticText( self.COM_OPTIONS, wx.ID_ANY, u"BaudR", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.BAUD_RATE_TITLE.Wrap( -1 )

		BAUD_R_SIZER.Add( self.BAUD_RATE_TITLE, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		COM_BAUD_RATE_CHOICEChoices = []
		self.COM_BAUD_RATE_CHOICE = wx.Choice( self.COM_OPTIONS, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, COM_BAUD_RATE_CHOICEChoices, 0 )
		self.COM_BAUD_RATE_CHOICE.SetSelection( 0 )
		BAUD_R_SIZER.Add( self.COM_BAUD_RATE_CHOICE, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )


		COM_OPTIONS_SIZER.Add( BAUD_R_SIZER, 1, wx.EXPAND, 5 )

		CONF_P_SIZER = wx.GridBagSizer( 0, 0 )
		CONF_P_SIZER.SetFlexibleDirection( wx.BOTH )
		CONF_P_SIZER.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.CONFIG_OPT_TITLE = wx.StaticText( self.COM_OPTIONS, wx.ID_ANY, u"ConfP", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.CONFIG_OPT_TITLE.Wrap( -1 )

		CONF_P_SIZER.Add( self.CONFIG_OPT_TITLE, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )

		COM_CONFIG_OPT_CHOICEChoices = []
		self.COM_CONFIG_OPT_CHOICE = wx.Choice( self.COM_OPTIONS, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, COM_CONFIG_OPT_CHOICEChoices, 0 )
		self.COM_CONFIG_OPT_CHOICE.SetSelection( 0 )
		CONF_P_SIZER.Add( self.COM_CONFIG_OPT_CHOICE, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )


		COM_OPTIONS_SIZER.Add( CONF_P_SIZER, 1, wx.EXPAND, 5 )


		self.COM_OPTIONS.SetSizer( COM_OPTIONS_SIZER )
		self.COM_OPTIONS.Layout()
		COM_OPTIONS_SIZER.Fit( self.COM_OPTIONS )
		self.DEVICES_CONN.AddPage( self.COM_OPTIONS, u"COM", True )
		self.NET_OPTIONS = wx.Panel( self.DEVICES_CONN, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.DEVICES_CONN.AddPage( self.NET_OPTIONS, u"NET", False )

		SYS_OPTIONS.Add( self.DEVICES_CONN, 1, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )


		GPSCPY_CC.Add( SYS_OPTIONS, 0, 0, 5 )

		BOTTOM_B_SIZER = wx.BoxSizer( wx.VERTICAL )

		self.BOTTOM_B_NOTBOOK = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.DATA_GRAPH_PANEL = wx.Panel( self.BOTTOM_B_NOTBOOK, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.DATA_GRAPH_PANEL.Enable( False )

		self.BOTTOM_B_NOTBOOK.AddPage( self.DATA_GRAPH_PANEL, u"Graph", True )
		self.DATA_TEXT = wx.Panel( self.BOTTOM_B_NOTBOOK, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.BOTTOM_B_NOTBOOK.AddPage( self.DATA_TEXT, u"Data", False )

		BOTTOM_B_SIZER.Add( self.BOTTOM_B_NOTBOOK, 1, wx.EXPAND|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )


		GPSCPY_CC.Add( BOTTOM_B_SIZER, 3, wx.EXPAND, 2 )


		self.SetSizer( GPSCPY_CC )
		self.Layout()
		self.STAT_B = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_ACTIVATE, self.GPSCPY_ACTIVE_EVENT )
		self.COM_DEVICES.Bind( wx.EVT_LISTBOX, self.COM_DEVICE_SELECTED_EVENT )
		self.COM_RELOAD_BUTTON.Bind( wx.EVT_BUTTON, self.COM_RELOAD_BUTTON_CLICK_EVENT )
		self.COM_CONNECT_BUTTON.Bind( wx.EVT_BUTTON, self.COM_CONNECT_BUTTON_CLICK_EVENT )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def GPSCPY_ACTIVE_EVENT( self, event ):
		event.Skip()

	def COM_DEVICE_SELECTED_EVENT( self, event ):
		event.Skip()

	def COM_RELOAD_BUTTON_CLICK_EVENT( self, event ):
		event.Skip()

	def COM_CONNECT_BUTTON_CLICK_EVENT( self, event ):
		event.Skip()


