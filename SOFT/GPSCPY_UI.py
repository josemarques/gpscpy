# -*- coding: utf-8 -*-

import wx
import GPSCPY_UIF

import GPSCPY_COM
COM_DATA_EVENT, COM_EVT_DATA = wx.lib.newevent.NewEvent()

class GPSCPYframe(GPSCPY_UIF.GPSCPY_FRAME_MAIN): 
	
	CURRENT_COM_DEVICE_SELECTED=""#Current com port selected on DEVICE_CONN_COM
	
	
	def __init__(self,parent): 
		GPSCPY_UIF.GPSCPY_FRAME_MAIN.__init__(self,parent)  
	"""
	def OnInit(self):
        GPSCPY_MAIN_FRAME=GPSCPY_UI.GPSCPY_FRAME_MAIN(None)
        
        panel = CanvasPanel(GPSCPY_MAIN_FRAME.DATA_GRAPH_PANEL)
        panel.draw()
        
        GPSCPY_MAIN_FRAME.Show(True)
        return True
	"""
	def GPSCPY_ACTIVE_EVENT(self,event): 
		"""
		num = int(self.m_textCtrl1.GetValue()) 
		self.m_textCtrl2.SetValue (str(num*num)) 
		"""
	def COM_RELOAD_BUTTON_CLICK_EVENT(self,event):
		LIST_COM_PORTS=GPSCPY_COM.COM_PORT_LIST()
		self.COM_DEVICES.Clear()
		for i in range(0,len(LIST_COM_PORTS)):
			self.COM_DEVICES.Append(LIST_COM_PORTS[i])
		
	def COM_DEVICE_SELECTED_EVENT(self, event):
		self.CURRENT_COM_DEVICE_SELECTED=self.COM_DEVICES.GetString(self.COM_DEVICES.GetSelection())
		print(self.CURRENT_COM_DEVICE_SELECTED)
		
	def COM_CONNECT_BUTTON_CLICK_EVENT(self,event):
		#CURRENT_COM_DEVICE_SELECTED
		
		CORRCON = False
		PORT = str(self.CURRENT_COM_DEVICE_SELECTED)
		BAUDR = 115200
		print("Connecting device:"+PORT+" with:"+str(BAUDR)+"bps")
		GPSCPY_COM.QUEUERXX.append(GPSCPY_COM.multiprocessing.Queue())
		GPSCPY_COM.QUEUETXX.append(GPSCPY_COM.multiprocessing.Queue())
		GPSCPY_COM.MRQCOM_DEVICE(PORT, BAUDR,
								GPSCPY_COM.QUEUERXX[len(GPSCPY_COM.QUEUERXX)-1],
								GPSCPY_COM.QUEUETXX[len(GPSCPY_COM.QUEUETXX)-1])
		print("Device connected")
		#SERIAL_PORT = serial.Serial(port, baud, timeout=0)
		#thread = threading.Thread(target=read_from_port, args=(serial_port,CORRCON,))
		#thread.start()


"""
#wx Event creation from threat
DataEvent, EVT_DATA = wx.lib.newevent.NewEvent()

def socket_thread(window):
...
#got some data
wx.PostEvent(window, DataEvent(data=data))
...

class myframe(wx.Frame):
def __init__(self, ...):
...
self.bind(EVT_DATA, self.OnData, self)
threading.Thread(target=socket_thread, args=(self,)).start()

def OnData(self, evt):
data = evt.data

"""
