import wx
import serial  # import PySerial libraries
import time

ser = serial.Serial("COM11", 57600, timeout=1)
ser.setDTR(0)
ser.setRTS(0)

OFF_COMMAND = '\x00'
START_BYTE = '\x80'
ID = '\x01'
COMMAND = '\x03'
SPEEDC = '\x01'
PARAM = '\x15'




def SetPos(SERVO,POSITION,SPEED):      
    setspeed = '%c%c%c%c%c' % (START_BYTE , ID, SPEEDC , SERVO , SPEED )
    ser.write(setspeed)
    Pos2 = POSITION
    Pos1 = 0
    if Pos2>128:
        Pos2-=128
        Pos1 = 1
    print ('Bit 7 %d Bit 8 %d' % (Pos1, Pos2))
    setpos = '%c%c%c%c%c%c' % (START_BYTE , ID , COMMAND , SERVO , chr(Pos1), chr(Pos2))
    ser.write(setpos)
  
def ServoOff(SERVO) :
    offcommand = '%c%c%c%c%c' % (START_BYTE , ID , OFF_COMMAND , SERVO , PARAM)
    ser.write(offcommand)

class MyPanel(wx.Panel):
  
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self.SetBackgroundColour("white")
        self.slider9 = wx.Slider(self, 0, 65, 0, 127, (10, 10), (400, 50),
            wx.SL_HORIZONTAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider1 = wx.Slider(self, 0, 127, 0, 254, (100, 80), (50, 250),
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider2 = wx.Slider(self, 0, 127, 0, 254, (200, 80), (50, 250),                                                                              
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider3 = wx.Slider(self, 0, 127, 0, 254, (300, 80), (50, 250),                                                                              
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider4 = wx.Slider(self, 0, 127, 0, 254, (400, 80), (50, 250),
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider5 = wx.Slider(self, 0, 127, 0, 254, (500, 80), (50, 250),
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider6 = wx.Slider(self, 0, 127, 0, 254, (600, 80), (50, 250),                                                                              
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider7 = wx.Slider(self, 0, 127, 0, 254, (700, 80), (50, 250),                                                                              
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)
        self.slider8 = wx.Slider(self, 0, 127, 0, 254, (800, 80), (50, 250),
            wx.SL_VERTICAL | wx.SL_AUTOTICKS | wx.SL_LABELS)                                                                             
        self.cb1 = wx.CheckBox(self, -1, 'Servo 1', (110, 350))
        self.cb1.SetValue(True)
        self.cb2 = wx.CheckBox(self, -1, 'Servo 2', (210, 350))
        self.cb2.SetValue(True)
        self.cb3 = wx.CheckBox(self, -1, 'Servo 3', (310, 350))
        self.cb3.SetValue(True)
        self.cb4 = wx.CheckBox(self, -1, 'Servo 4', (410, 350))
        self.cb4.SetValue(True)
        self.cb5 = wx.CheckBox(self, -1, 'Servo 5', (510, 350))
        self.cb5.SetValue(True)
        self.cb6 = wx.CheckBox(self, -1, 'Servo 6', (610, 350))
        self.cb6.SetValue(True)
        self.cb7 = wx.CheckBox(self, -1, 'Servo 7', (710, 350))
        self.cb7.SetValue(True)
        self.cb8 = wx.CheckBox(self, -1, 'Servo 8', (810, 350))
        self.cb8.SetValue(True)
        self.Bind(wx.EVT_SLIDER, self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb1.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb2.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb3.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb4.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb5.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb6.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb7.GetId(), self.ServoUpdate)
        wx.EVT_CHECKBOX(self, self.cb8.GetId(), self.ServoUpdate)

    def ServoUpdate(self, event):  
        self.pos1 = self.slider1.GetValue()
        self.pos2 = self.slider2.GetValue()
        self.pos3 = self.slider3.GetValue()
        self.pos4 = self.slider4.GetValue()
        self.pos5 = self.slider5.GetValue()
        self.pos6 = self.slider6.GetValue()
        self.pos7 = self.slider7.GetValue()
        self.pos8 = self.slider8.GetValue()
        self.speed = self.slider9.GetValue()
        if self.cb1.GetValue():
            ServoOff(0)
        else: SetPos(0,self.pos1,self.speed)
        if self.cb2.GetValue():
            ServoOff(1)
        else: SetPos(1,self.pos2,self.speed)
        if self.cb3.GetValue():
            ServoOff(2)
        else: SetPos(2,self.pos3,self.speed)
        if self.cb4.GetValue():
            ServoOff(3)
        else: SetPos(3,self.pos4,self.speed)
        if self.cb5.GetValue():
            ServoOff(4)
        else: SetPos(4,self.pos5,self.speed)
        if self.cb6.GetValue():
            ServoOff(5)
        else: SetPos(5,self.pos6,self.speed)
        if self.cb7.GetValue():
            ServoOff(6)
        else: SetPos(6,self.pos7,self.speed)
        if self.cb8.GetValue():
            ServoOff(7)
        else: SetPos(7,self.pos8,self.speed)
    
        

app = wx.PySimpleApp()

frame = wx.Frame(None, -1, "Simple Servo Controller", size = (950, 440))

MyPanel(frame,-1)

frame.Show(True)

app.MainLoop()
