#!/usr/bin/env python
import threading
#import multiprocessing
from multiprocessing import Pool, Queue, Process
import time
#from multiprocessing import queue
import queue
import serial
import serial.tools.list_ports

import wx

#MULTT THREAD LIST CONTAINERS
PRC=[]
TRH=[]
SERIALDEV=[]
PRC_COMID=[]
QUEUERXX=[]#RX PROGRAM->DEVICE
QUEUETXX=[]#TX DEVICE->PROGRAM
#MULTT THREAD LIST CONTAINERS

def COM_PORT_LIST():

        ports = serial.tools.list_ports.comports()
        PORT_DEVICES=[]
        for p in ports:
                #print(p.device)
                PORT_DEVICES.append(str(p.device))
        #print(len(ports), 'ports found')
        return PORT_DEVICES

def COM_INFO_LIST():
        return (serial.tools.list_ports.comports())

############MRQS MULTYPROCESS COM_PROTOCOL

def MRQCOM_INIT(PORT, BAUDR):
        ser = serial.Serial(PORT, BAUDR,timeout=0)
        return ser
           
def MRQCOM_READ(SERIAL):#Reads datalines
        return SERIAL.readline().decode()

def MRQCOM_WRITE(SERIAL, TXDATA):#Write datalines
        txdata=str(TXDATA)+"\n"
        SERIAL.write(txdata.encode())

        
def MRQCOM_THREAT_LOOP(SERIAL,QUEUERX,QUEUETX):
        
        RXDATA=""
        while (not (RXDATA=="KILL")):
                #SEND DATA IN TX_QUEUE
                #print("MRQCOM_THREAT_LOOP")
                if(not QUEUERX.empty()):#If some data is in the queue to be recieved by the device
                        #print("COMSEND:"+str(RXDATA))
                        RXDATA= QUEUERX.get()
                        MRQCOM_WRITE(SERIAL, RXDATA)
                #SEND DATA IN TX_QUEUE
            
                #RECIVE DATA AND PUTS IT ON RXQUEUE
                #print("test")
                TXDATA = MRQCOM_READ(SERIAL)
                #print("COMRECIV:"+str(TXDATA))
                QUEUETX.put(TXDATA)               
              

def MRQCOM_THREAD(PORT, BAUDR, QUEUERX, QUEUETX):
        print("MRQCOM_DEVICE")
        MRQ_SERIAL=MRQCOM_INIT(PORT, BAUDR)
        THREAD = threading.Thread(target=MRQCOM_THREAT_LOOP, args=(MRQ_SERIAL,QUEUERX,QUEUETX,))
        THREAD.start()
        
        #THREAD = multiprocessing.Process(target=MRQCOM_THREAT_LOOP, args=(MRQ_SERIAL,QUEUERX,QUEUETX,BCON))
        #THREAD.start()

        #multiprocessing.Process(target=MRQCOM_THREAT_LOOP, args=(MRQ_SERIAL,QUEUERX,QUEUETX)).start()
        
        #print("MRQCOM_DEVICE:THREAD::")
        #print(QUEUERX.get())
        #print(QUEUETX.get())
        
        #THREAD.terminate() 
        #SERIAL_PORT = serial.Serial(port, baud, timeout=0)
        #THREAD = threading.Thread(target=read_from_port, args=(serial_port,CORRCON,))
        #THREAD.start()  
        return(thread)
############MRQS MULTYPROCESS COM_PROTOCOL


####MULTI PROCESS# DEV
import multiprocessing
import time
     

####MULTI PROCESS# DEV


####UNITY TESTs

##BASE FUNCTIONALITY
"""

COMLIST=COM_PORT_LIST()
print("AVALIABLE PORTS:")
print(COMLIST)
print()

print("Select port:")

PORTSELECT=input()

COMDEVICE=MRQCOM_INIT(PORTSELECT,115200)

while 1:
        DATARX=MRQCOM_READ(COMDEVICE)
        print(DATARX)
        
        DATATX=input("SEND COMMAND:")
        
        MRQCOM_WRITE(COMDEVICE, DATATX)#Write datalines
        
#"""
##BASE FUNCTIONALITY 

##MULTY THREAD FUNCTIONALITY


if __name__ == '__main__':

        while 1:

                COMLIST=COM_PORT_LIST()
                print("AVALIABLE PORTS:")
                print(COMLIST)
                print()

                print("Select port:")

                PORTSELECT=input()

                if(len(PORTSELECT)>3):#CREATE SERIAL THREAD
                        SERIALDEV.append(MRQCOM_INIT(PORTSELECT, 115200))
                        PRC_COMID.append(PORTSELECT)
                        QUEUERXX.append(Queue())
                        QUEUETXX.append(Queue())
                        PRC.append(threading.Thread(target=MRQCOM_THREAT_LOOP, args=(SERIALDEV[len(SERIALDEV)-1],QUEUERXX[len(QUEUERXX)-1],QUEUETXX[len(QUEUETXX)-1])))
                        PRC[len(PRC)-1].start()
                        
                for i in range(0, len(PRC_COMID)):#READ-WRITE TO ALL COM DEVICES
                        COMRX=input("SEND DATA TO:"+str(PRC_COMID[i])+":")
                        QUEUERXX[i].put(COMRX)
                        COMTX=QUEUETXX[i].get()
                        print("RECIEVED:"+str(COMTX))







#"""
"""
connected = False
port = 'COM11'
baud = 9600

serial_port = serial.Serial(port, baud, timeout=0)

def handle_data(data):
    print(data)

def read_from_port(ser):

        while True:
           print("test")
           reading = ser.readline().decode()
           handle_data(reading)

thread = threading.Thread(target=read_from_port, args=(serial_port,))
thread.start()
#"""

#####  EXTERNAL THREAD MULTIPLE COMPORTS



#####  EXTERNAL THREAD MULTIPLE COMPORTS

#!/usr/bin/env python


"""
from multiprocessing import Pool, Queue, Process
import time

def f(q_in, q_out): ##PROCESS LOOP
   # Create unique ID to clearly distinguish between workers:
    u = time.time()
    while True:
        m = q_in.get()
        print (str(u)+":::"+str(m))
        if m == 'q':
                print("KILL::"+str(u))
                break
        else:
            q_out.put(-m)

if __name__ == '__main__':
        #Variable definition
        N = 4
        q_in=[]
        q_out =[]
        p=[]

        #Generation
        for i in range(0, N):
                q_in.append( Queue())
                q_out.append( Queue())
            
                    ####p = Pool(N, f, (q_in, q_out))
                p .append( multiprocessing.Process(target=f, args=(q_in[i],q_out[i])))
                p[i].start()
        #Generation
        M = 1000
        for ii in range(0, M):
                    # Send input:
                
                for i in range(0, N):
                        q_in[i].put(ii)

                    # Get output:
                #for ii in range(0, M):
                        m = q_out[i].get()
                        print (m)
                
                    # Tell workers to quit:
        
        q_in[i].put('q')
"""
                
##MULTY THREAD FUNCTIONALITY

        
####UNITY TESTs


############################################################EXAMPLES
"""           
class COM_DEVICE(queue.QRX,queue.QTX,COMDIR,BAUDR=9800,TIMEOUT=1):
        CORRCON = False
        SERIAL_PORT = None
        #COMDIR = str(CURRENT_COM_DEVICE_SELECTED)
        #COMDIR="COM11"
        #BAUD = 115200

        #SERIAL_PORT = serial.Serial(port, baud, timeout=0)
        #thread = threading.Thread(target=read_from_port, args=(serial_port,CORRCON,))
        #thread.start()
        
        def __init__(self, qrx, qtx):
                SERIAL_PORT = INIT_COM(COMDIR, BAUDR)#serial.Serial(COMDIR, BAUDR, TIMEOUT)
                if
"""




"""#wx Event creation from threat
DataEvent, EVT_DATA = wx.lib.newevent.NewEvent()

def socket_thread(window):
...
        #got some data
        wx.PostEvent(window, DataEvent(data=data))
...

class myframe(wx.Frame):
        def __init__(self, ...):
                ...
                self.bind(EVT_DATA, self.OnData, self)
                threading.Thread(target=socket_thread, args=(self,)).start()
                ...
                ...
        def OnData(self, evt):
                data = evt.data
                ...
        """

"""#Queue handling

from queue import Queue 
from threading import Thread, Event 
  
# A thread that produces data 
def producer(out_q): 
    while running: 
        # Produce some data 
        ... 
        # Make an (data, event) pair and  
        # hand it to the consumer 
        evt = Event() 
        out_q.put((data, evt)) 
        ... 
        # Wait for the consumer to process the item 
        evt.wait() 
      
    # A thread that consumes data 
def consumer(in_q): 
        while True: 
            # Get some data 
            data, evt = in_q.get() 
            # Process the data 
            ... 
            # Indicate completion 
            evt.set() 

"""





"""
def handle_data(data):
    print(data)

def read_from_port(ser,BCON):
    connected=BCON
    while not connected:
        #serin = ser.read()
        connected = True

        while True:
           #print("test")
           reading = ser.readline().decode()
           handle_data(reading)
           
#COMDIR = str(CURRENT_COM_DEVICE_SELECTED)
#COMDIR="COM11"
#BAUD = 115200

#SERIAL_PORT = serial.Serial(port, baud, timeout=0)
#thread = threading.Thread(target=read_from_port, args=(serial_port,CORRCON,))
#thread.start()     

"""
